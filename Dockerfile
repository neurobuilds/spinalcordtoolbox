ARG BASE_DISTRO=ubuntu
ARG BASE_VERSION=20.04
ARG PACKAGE_MANAGER=apt
ARG SCT_VERSION=5.8

FROM ${BASE_DISTRO}:${BASE_VERSION}
ARG DEBIAN_FRONTEND=noninteractive
ARG BASE_DISTRO
ARG BASE_VERSION
ARG PACKAGE_MANAGER
ARG SCT_VERSION

WORKDIR /opt

RUN apt-get update && \
    apt-get install -y wget git bzip2 gcc libglib2.0-0 && \
    git clone https://github.com/spinalcordtoolbox/spinalcordtoolbox sct && \
    cd sct && \
    git checkout tags/${SCT_VERSION} && \
    ./install_sct -y -i

ENV PATH /opt/sct/bin:$PATH